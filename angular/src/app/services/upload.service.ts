import {Injectable} from '@angular/core';
import {GLOBAL} from './global';

@Injectable()
export class UploadService{
	public url:string;

	constructor(){
		this.url = GLOBAL.url;
	}

	makeFileRequest(url:string,params:Array<string>,files:Array<File>,token: string,name: string){
		return new Promise(function(resolve,reject){
			var formData: any = new FormData();
			var xhr = new XMLHttpRequest();

			for(var i=0;i<files.length;i++){
				formData.append(name,files[i],files[i].name);
			}

			xhr.onreadystatechange = function () {
				if(xhr.readyState == 4){
					if(xhr.status == 200){
						console.log("OK PROMISE");
						resolve(JSON.parse(xhr.response));
					}else{
						console.log("NO PROMISE");
						reject(xhr.response);
					}
				}
			}

			xhr.open('POST',url,true);
			xhr.setRequestHeader('authorization',token);
			xhr.send(formData);
		});
	}
}