'use strict';

let mongoose = require('mongoose');
var app = require('./app');
var port = 3800;



mongoose.Promise = global.Promise;
mongoose.connect('mongodb://localhost:27017/crud',{ useNewUrlParser: true })
	.then(() => {
		console.log('-- La conexión a la base de datos');
	
		app.listen(port,()=>{
			console.log("Servidor corriendo en localhost");
		});
	}).catch(err => console.log(err)); 