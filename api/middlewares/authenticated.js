var jwt = require('jwt-simple');
var moment = require('moment');
var secret = 'JesusFuentesCeancas';

module.exports.ensureAuth = function(req,res,next){
	if(!req.headers.authorization){
		return res.status(403).send({mesaage:'La petición no tiene la cabecera de autenticación'});
	}

	var token = req.headers.authorization.replace(/['"]+/g,'');

	try{
		// Sensible a excepciones
		var payload = jwt.decode(token,secret);	
		
		if(payload.exp <= moment().unix()){
			return res.status(401).send({
				message:'El Token ha expirado'
			});
		}
	}catch(ex){
		return res.status(404).send({
				message:'El Token no es válido'
			});
	}
	
	// DEVUELVE USER Y SE ALAMACENA EN EL REQUEST
	req.user = payload;

	next();
}