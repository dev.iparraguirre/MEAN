'use strict'

var bcrypt = require('bcrypt-nodejs');
var mongoosePaginate = require('mongoose-pagination');
var fs = require('fs');
var path = require('path');

var User = require('../models/user');
var Follow = require('../models/follow');
var Publication = require('../models/publication');

var jwt = require('../services/jwt');
//rutas
function home(req,res){
	res.status(200).send({
		message:'Hola Mundo'
	});
};


function pruebas(req,res){
	res.status(200).send({
		message:'Accion de pruebas en el servidor NodeJS'
	});
};

//Regitro
function saveUser(req,res){
	var params = req.body;
	var user = new User();
	 
	console.log(req.body);

	if(params.name &&  params.surname &&
		params.nick && params.email && params.password){

		user.name = params.name;
		user.surname = params.surname;
		user.nick = params.nick;
		user.email = params.email;
		user.role = 'ROLE_USER';
		user.image = null;

		//Controlar usuarios duplicados
		User.findOne({$or: [
							{email: user.email.toLowerCase()},
							{nick: user.nick.toLowerCase()}
						]
					}).exec(function (err, users) {
			console.log("TESTING V2.0");
			if(err) return res.status(500).send({message:'Error en la petición de usuarios'});

			//SINO SE DEBE CUBRIR UNA ACCION PORQUE NO EXISTE RESPONSE
			//Error: Can't set headers after they are sent.
			if(users && users._id != ''){
				return res.status(200).send({message:'El usuario que intentas registrar ya existe'});
			}else{
				//Cifrado de password
				bcrypt.hash(params.password,null,null,(err,hash)=>{
					user.password = hash;
					user.save((err,userStored)=>{
						if(err) return  res.status(500).send({ message:'Error al guardar el usuario' })
						if(userStored){
							res.status(200).send({ user: userStored });
						}else{
							res.status(404).send({ message:'No se ha registrado el usuario' });
						}
					})	
				});	
			} 
		});
		

	}else{
		res.status(200).send({
			message:'Envia todos los campos necesarios!!'
		})
	}
};

//Login
function loginUser(req,res){
	var params = req.body;

	var email = params.email;
	var password = params.password;

	User.findOne({ email:email},(err,user)=>{
		if(err) return res.status(500).send({message:'Error en la petición de usuarios'});

		if(user){
			bcrypt.compare(password,user.password,(err,check)=>{
				if(check){
					if(params.gettoken){
						//devolver token
						return res.status(200).send({
							token:jwt.createToken(user),
						});
					}else{
						//devolver datos de ususario
						user.password = undefined;
						return res.status(200).send({user});
					}
					
				}else{
					return res.status(404).send({ message:'El usuario no se ha podido ientificar' });
				}
			})
		}else{
			return res.status(500).send({message:'No existe el usuario'});
		}
	});
}

function getUser(req,res){
	// body por post put params get
	var userId = req.params.id;

	User.findById(userId, (err,user)=>{
		if(err) return res.status(500).send({message:'Error en la petición'})
		
		if(!user) return res.status(404).send({message:'El usuario no existe'});

		
		/*followThisUser(req.user.sub,userId).then((value)=>{
			console.log(value);
			return res.status(200).send({user:user,value});
		});*/
		user.password = undefined;
		// AQUI DEBERÏA IR LA LLAMADA A LA FUNCION ASYNC
		
		Follow.findOne({'user':req.user.sub,'followed':userId}).exec((err,followed)=>{
		if(err) return res.status(500).send({message:'Error en la petición'})
		
		//if(!followed) return res.status(404).send({message:'El usuario no existe'});
	
			Follow.findOne({'followed':req.user.sub,'user':userId}).exec((err,following)=>{
				if(err) return res.status(500).send({message:'Error en la petición'})
		
				//if(!following) return res.status(404).send({message:'El usuario no existe'});
				

				return res.status(200).send({user,following,followed});
				
			});
		});

		
	});

}

async function followThisUser(identity_user_id,user_id){
	console.log(identity_user_id);
	console.log(user_id);
	
 	await Follow.findOne({'user':identity_user_id,'followed':user_id}).exec((err,follow)=>{
		if(err) return handleError(err);
		Follow.findOne({'followed':identity_user_id,'user':user_id}).exec((err,following)=>{
			if(err) return handleError(err);
			
			var attr = {
				followed:follow,
				following:following
			}
			
			return attr;
		});
	});

	/*const followed = await Follow.findOne({'user':user_id,'followed':identity_user_id}).exec((err,follow)=>{
		if(err) return handleError(err);
		console.log(follow);
		return follow;
	});

	console.log(following);
	console.log(followed);

	return {
		followed:followed,
		following:following
	}*/
}	


function getUsers(req,res){
	//console.log(req.user);
	var identity_user_id = req.user.sub;
	
	var page = 1;
	if(req.params.page){
		page = req.params.page;
	}

	var itemsPerPage = 5;

	User.find().sort('_id').paginate(page,itemsPerPage,function(err,users,total){
		if(err) return res.status(500).send({message:'Error en la petición'})
		
		if(!users) return res.status(404).send({message:'Los usuarios no existen'});

		//START
		Follow.find({'user':identity_user_id}).select({'_id':0, '__v':0,'user':0}).exec((err,following) =>{
			if(err) return res.status(500).send({message:'Error en la petición'})	

			var following_clean = [];
			
			following.forEach((follow)=>{
				following_clean.push(follow.followed);	
			});
			//START
			Follow.find({'followed':identity_user_id}).select({'_id':0, '__v':0,'followed':0}).exec((err,followed) =>{
				if(err) return res.status(500).send({message:'Error en la petición'})
				
				var followed_clean = [];
				
				followed.forEach((follow)=>{
					followed_clean.push(follow.user);	
				});

				return res.status(200).send({
					users,
					users_following_me:following_clean,
					users_followed:followed_clean,
					total,
					pages:Math.ceil(total/itemsPerPage)
				});
			});
		});
		
	});

}


async function followUserIds(user_id){
	var following = null; 
	await Follow.find({'user':user_id}).select({'_id':0, '__v':0,'user':0}).exec((err,follows) =>{
		//console.log(follows);
		var follows_clean = [];
		
		follows.forEach((follow)=>{
			follows_clean.push(follow.followed);	
		});

		return follows_clean;
	});

	var followed = null; 
	await Follow.find({'followed':user_id}).select({'_id':0, '__v':0,'followed':0}).exec((err,follows) =>{
		//console.log(follows);
		var follows_clean = [];
		
		follows.forEach((follow)=>{
			follows_clean.push(follow.user);	
		});

		return follows_clean;
	});

	
	console.log(followed);
	console.log(following);

	return {
		following:following,
		followed:followed
	}
}

// Edición de datos de usuario
function updateUser(req,res){
	var userId = req.params.id;
	var update = req.body;

	//borrar propiedad password
	delete update.password;

	if(userId != req.user.sub){
		return res.status(500).send({message:'No cuenta con permisos para actualizar este usuario'})
	}else{

		//REVISAR PORQUE SE ACTUALIZA CUNDO SE REPITE EL NICK
		User.find({$or: [
							{email: update.email.toLowerCase()},
							{nick: update.nick.toLowerCase()}
						]
					}).exec((err,users)=>{
						var user_isset = false;
						console.log(users);
						users.forEach((user)=>{
							if(user && user._id != userId)  user_isset = true;
						});

						if(user_isset)
							return res.status(500).send({message:'Los datos ya estan en uso'});

						User.findByIdAndUpdate(userId,update,{new:true},(err,userUpdated)=>{
							if(err) return res.status(500).send({message:'Error en la petición'})
						
							if(!userUpdated) return res.status(404).send({message:'No se ha podido actualizar el ususario'});

							//Objeto Original userUpdated
							return res.status(200).send({user:userUpdated});

						});

					});

		//Object new true devuelve uno nuevo
		
	}
}


function uploadImage(req,res){
	var userId = req.params.id;

	if(req.files){
		var file_path = req.files.image.path; 
		console.log(file_path);
		var file_split = file_path.split('\\');

		var file_name = file_split[2];
		console.log(file_name);

		var ext_split = file_name.split('\.');
		var file_ext = ext_split[1];
		console.log("NO HAY EXT "+file_ext);

		if(userId != req.user.sub){
			// IMPORTANTE NECESITAMOS EL RETURN RESPONES
			return removeFilesOfUploads(res,file_path,'No cuenta con permisos para actualizar este usuario');
		}
		
		if(file_ext  == 'png' || file_ext  == 'jpeg' || file_ext  == 'jpg' || file_ext  == 'gif'){
			User.findByIdAndUpdate(userId,{image:file_name},{new:true},(err,userUpdated)=>{
				if(err) return res.status(500).send({message:'Error en la petición'})
		
				if(!userUpdated) return res.status(404).send({message:'No se ha podido actualizar el ususario'});

				//Objeto Original userUpdated
				return res.status(200).send({user:userUpdated});

			})
		}else{
			return removeFilesOfUploads(res,file_path,'Extensiones incorrectas');
		}
		
	

	}else{
		return res.status(200).send({message:'No cuentas con el campo file'});
	}
}

function removeFilesOfUploads(res,file_path,message){
	console.log("REMOVE FILES UPLOAD");
	console.log(file_path);
	fs.unlink(file_path,(err)=>{
		return res.status(200).send({message:message});
	});
}

function getImageFile(req,res){
	var image_file = req.params.imageFile;
	var path_file = './uploads/users/'+image_file;

	fs.exists(path_file,(exists) => {
		if(exists){
			res.sendFile(path.resolve(path_file));
		}else{
			return res.status(200).send({message:'No existe la imagen'});	
		}
	})
}

function getCounters(req,res){
	let userId = req.user.sub;

	if(req.params.id){
		userId = req.params.id;
	}
	
	Follow.count({'user':userId}).exec((err,countfw)=>{
		if(err) return handleError(err);

		Follow.count({'followed':userId}).exec((err,countfwing)=>{
			if(err) return handleError(err);

			Publication.count({'user':userId}).exec((err,countpub)=>{
				if(err) return handleError(err);
					return res.status(200).send({
						following:countfwing,
						followed:countfw,
						publication:countpub,
					});
			});

				
		});
	});
	
}

async function getCountFollow(user_id){
	var following = await Follow.count({'user':user_id}).exec((err,count)=>{
		if(err) return handleError(err);

		return count;
	});

	var followed = await Follow.count({'followed':user_id}).exec((err,count)=>{
		if(err) return handleError(err);

		return count;
	});

	return {
		followed,
		following
	}
}

module.exports={
	home,
	pruebas,
	saveUser,
	loginUser,
	getUser,
	getUsers,
	updateUser,
	uploadImage,
	getImageFile,
	getCounters
}