'use strict'

const express = require('express');
var UserController = require('../controllers/user');

var apis = express.Router();
var md_auth = require('../middlewares/authenticated');

var multipart  = require('connect-multiparty');
var md_upload = multipart({uploadDir:'./uploads/users'})

apis.get('/home',UserController.home);
apis.get('/pruebas',md_auth.ensureAuth,UserController.pruebas);
apis.post('/register',UserController.saveUser);
apis.post('/login',UserController.loginUser);
apis.get('/user/:id',md_auth.ensureAuth,UserController.getUser);
apis.get('/users/:page?',md_auth.ensureAuth,UserController.getUsers);
apis.put('/update-user/:id',md_auth.ensureAuth,UserController.updateUser);
apis.post('/upload-image-user/:id',[md_auth.ensureAuth,md_upload],UserController.uploadImage);
apis.get('/counters/:id?',md_auth.ensureAuth,UserController.getCounters);
apis.get('/get-image-user/:imageFile',UserController.getImageFile);

module.exports = apis;