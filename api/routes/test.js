'use strict'

const express = require('express');
var TestController = require('../controllers/test');

var api = express.Router();
var md_auth = require('../middlewares/authenticated');

var multipart  = require('connect-multiparty');
var md_upload = multipart({uploadDir:'./uploads/users'})

api.get('/test',md_auth.ensureAuth,TestController.test);
api.get('/prueba',md_auth.ensureAuth,TestController.inicio);

module.exports = api;